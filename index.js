// Bài 1:
function createTableVeryNice() {
    var rows = '';
    var myTable = document.getElementById('here');
    for (var i = 1; i <= 100; i += 10) {
        var columns = '';
        for (var j = 0; j < 10; j++) {
            var column =
                `
            <td>
            ${i + j} 
            </td>
            `
            columns += column;
        }
        var row =
            `
            <tr>
                ${columns}
            </tr>
            `
        rows += row;
    }
    myTable.innerHTML = rows;

}
// Bài 2
var arrSoNguyen = [];
function taoMangSoNguyen() {
    var input = document.getElementById('txt-mang-so-nguyen');
    arrSoNguyen.push(input.value * 1);
    document.getElementById('in-mang').innerHTML = arrSoNguyen;
    input.value = '';
}
function handleKeyPress(event, myFunction) {
    if (event.keyCode == 13) {
        // addNumber();
        myFunction();
        event.preventDefault();
    }
}
function laSoNguyenTo(n) {
    if (n < 2) {
        return false;
    }

    // Neu n = 2 la SNT
    if (n === 2) {
        return true;
    }

    // Neu n la so chan thi ko phai la SNT
    if (n % 2 === 0) {
        return false;
    }

    // Lap qua cac so le
    for (var i = 3; i < (n - 1); i += 2) {
        if (n % i === 0) {
            return false;

        }
    }

    return true;

}
function inRaMangSoNguyenTo() {
    var arrSoNguyenTo = [];
    if (arrSoNguyen) {
        arrSoNguyen.forEach((soNguyen) => {
            if (laSoNguyenTo(soNguyen)) {
                arrSoNguyenTo.push(soNguyen);
            }
        })
        document.getElementById('in-ntn').innerHTML = arrSoNguyenTo;
        return;
    }



}
// Bài 3:
function tinhTongS() {
    var n = document.getElementById('txt-nhap').value * 1;
    document.getElementById('txt-nhap').value = '';
    var sum = 0;
    for (var i = 2; i <= n; i++) {
        sum += i;
    }
    sum += 2 * n;
    document.getElementById('result').innerHTML = `n là: ${sum}`;

}
// Bài 4:
function tinhSoLuongUocSo() {
    var n = document.getElementById('txt-nhap4').value * 1;
    var mangUocSo = [];
    // console.log('n: ', n);
    document.getElementById('txt-nhap4').value = '';
    for (var i = 1; i <= n; i++) {
        if (n % i == 0) {
            mangUocSo.push(i);
        }
    }
    document.getElementById('result4').innerHTML =
        `Số ${n} có ${mangUocSo.length} ước số bao gồm: ${mangUocSo}`
        ;
}
// Bài 5:
function timSoDaoNguoc() {
    var n = document.getElementById('txt-nhap5').value * 1;
    document.getElementById('txt-nhap5').value = '';
    var myString = n.toString();
    var newString = '';
    for (var i = myString.length - 1; i >= 0; i--) {
        newString += myString[i];
    }
    newString = newString * 1;
    // console.log('newString: ', newString);
    document.getElementById('result5').innerHTML = newString;
}
// Bài 6:
function timx() {
    var sum = 0;
    var result = 0;
    var resultEl = document.getElementById('result6');
    for (var i = 1; sum <= 100; i++) {
        sum += i;
        result = i;
    }
    resultEl.innerHTML = `x là: ${result}`;
}
// bài 7
function inBangCuuChuong() {
    var n = document.getElementById('txt-nhap7').value * 1;
    document.getElementById('txt-nhap7').value = '';
    var rows = '';
    for (var i = 0; i <= 10; i++) {
        var row =
            `
        <div>
        ${n} x ${i} = ${n * i}
        </div>
        `
        rows += row;
    }
    document.getElementById('result7').innerHTML = rows;
}
// bài 8
function chiaBai() {
    var players = [[], [], [], []];
    var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];
    //! tổng số lá bài là 12
    // ! có 4 người chơi---> mỗi người 3 lá
    // !người thứ 1 sẽ nhận lá thứ 1, 5, 9
    // ! người thứ 2 sẽ nhận lá thứ 2, 6, 10
    //! người thứ 3 sẽ nhận lá thứ 3, 7 , 11
    // ! người thứ 4 sẽ nhận lá thứ 4, 8, 12
    //! ==> người thứ i sẽ nhận lá thứ i, i+ 4, i +4 +4
    //todo: duyệt qua mỗi người
    for (var i = 0; i < players.length; i++) {
        //todo: duyệt qua lá bài
        for (var j = 0; j < cards.length; j += 4) {
            players[i].push(cards[j + i])
        }
    }
    var rows = '';
    for (var i = 0; i < players.length; i++) {
        var row =
            `
        <div>
       người chơi thứ ${i + 1}: ${players[i]}    
        </div>
        `
        rows += row;
    }
    document.getElementById('result8').innerHTML = rows;
}
// bài 9 
function timSoGaSoCho() {
    var tongSoCon = document.getElementById('txt-tong-con').value * 1;
    var tongSoChan = document.getElementById('txt-tong-chan').value * 1;
    document.getElementById('txt-tong-con').value = '';
    document.getElementById('txt-tong-chan').value = '';
    // ? đề cho : tổng gà(x) + tổng chó(y) = tổng con vật,
    // ? tổng chân gà(x*2)  + tổng chân chó(y*4) = tổng chân con vật
    // ? tìm số con gà, con chó ---> hệ phương trình
    // todo: giải:
    // todo: y = (tổng chân con vật - tổng con vật*2) / 2;
    var soCho = (tongSoChan - tongSoCon * 2) / 2;
    var soGa = tongSoCon - soCho;
    var result = document.getElementById('result9');
    result.innerHTML =
        `
        Số con chó là: ${soCho}
        <br/>   
        Số con gà là: ${soGa}
        `
}
// Bài 10: tính góc đồng hồ
function tinhGoc() {
    var soGio = document.getElementById('txt-gio').value * 1;
    var soPhut = document.getElementById('txt-phut').value * 1;
    if (soGio > 12 || soPhut > 60) {
        alert("Sai định dạng");
        return;
    }
    document.getElementById('txt-gio').value = '';
    document.getElementById('txt-phut').value = '';

    var gocMoiGio = 360 / 12;
    var gocMoiPhut = 360 / 60;
    var gocPhut = gocMoiPhut * soPhut;

    var gocGio = gocMoiGio * soGio;
    // ! vì kim phút di chuyển thì kim giờ di chuyển theo -->
    var gocTaoThem = soPhut / 60 * gocMoiGio;
    gocGio += gocTaoThem;
    var gocCanTim = gocGio - gocPhut;
    gocCanTim = Math.abs(gocCanTim);
    gocCanTim = Math.min(360 - gocCanTim, gocCanTim);
    var result = document.getElementById('result10');
    result.innerHTML = `góc của ${soGio}h:${soPhut}' là: ${gocCanTim}°`;
}